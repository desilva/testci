# these are settings that setupATLAS -3 will set

if [ "$1" = "python3" ]; then
    export PANDA_PY3=1
    export RUCIO_PYTHONBIN=python3
    export EMI_PYTHONBIN=python3
else
    unset PANDA_PY3
#    unset RUCIO_PYTHONBIN
#    unset EMI_PYTHONBIN
fi

