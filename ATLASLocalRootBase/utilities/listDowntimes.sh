#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! listDowntimes.sh
#!
#! list ATLAS downtimes
#!
#! Usage: 
#!     listDowntimes.sh <site>
#!
#! History:
#!   16Dec20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# -*- coding: utf-8 -*- 
"exec" "$( [ ! -z $EMI_PYTHONBIN ] && \echo $EMI_PYTHONBIN || \echo /usr/bin/python)" "-u" "-Wignore" "$0" "$@"

from __future__ import print_function 

import sys, os, json

n_arguments = len(sys.argv) - 1
if n_arguments != 1:
    print('Error: need 1 arg (site) for listDowntimes')
    sys.exit(64)
site = sys.argv[1]

try:  
  jsonfileDir = os.environ['ALRB_tmpScratch'] + '/cric/'  
except: 
  sys.exit(64)

command= os.environ['ATLAS_LOCAL_ROOT_BASE'] + '/utilities/getCricData.sh -q downtime ' + jsonfileDir  
try:
  return_value = os.system(command)  
  if return_value != 0:
      sys.exit(64)
except: 
  sys.exit(64)

jsonfileLocation = jsonfileDir + 'downtime.json'

with open(jsonfileLocation,'r') as jsonfile:
  jsondata = json.load(jsonfile)
jsonfile.close()

try:
    siteInfo= 'Services: ' + jsondata[site][0]['affected_services'] + '\nDescription: ' + jsondata[site][0]['description'] + '\nInfo: ' + jsondata[site][0]['info_url'] + '\nStart: ' + jsondata[site][0]['start_time'] + '\nEnd: ' + jsondata[site][0]['end_time']
except:
    siteInfo= 'site not in downtime'

print(siteInfo) 

sys.exit(0)
