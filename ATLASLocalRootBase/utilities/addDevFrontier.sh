#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! addDevFrontier.sh
#!
#! modify Frontier to use development servlet
#!
#! Usage:
#!     addDevFrontier.sh
#!
#! History:
#!    14May20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# default return code is to fail
let alrb_retCode=64

alrb_result=`\echo $ALRB_testPath | \grep -e ",devatlr,"`
if [ $? -ne 0 ]; then
    exit $alrb_retCode
fi

if [[ ! -z $ALRB_noFrontierSetup ]] && [[ "$ALRB_noFrontierSetup" = "YES" ]]; then
    exit $alrb_retCode
fi

export FRONTIER_SERVER="(serverurl=http://atlasfrontier-ai.cern.ch:8000/devatlr)`\echo $FRONTIER_SERVER | \sed -e 's|(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)||g' -e 's|(serverurl=http://atlasfrontier-ai.cern.ch:8000/devatlr)||g' | \sed -e 's|\(-ai.cern.ch:[[:digit:]]*\)\/atlr|\1/devaltr|g'`"
alrb_retCode=$?


\echo $FRONTIER_SERVER
exit $alrb_retCode
