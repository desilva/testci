#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! listDDMBlacklists.sh
#!
#! simple list of ddm blacklistings
#!
#! Usage: 
#!     listDDMBlacklists.sh
#!
#! History:
#!   16Dec20: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

# -*- coding: utf-8 -*- 
"exec" "$( [ ! -z $EMI_PYTHONBIN ] && \echo $EMI_PYTHONBIN || \echo /usr/bin/python)" "-u" "-Wignore" "$0" "$@"

from __future__ import print_function 

import sys, os
import json

try:  
  jsonfileDir = os.environ['ALRB_tmpScratch'] + '/cric/'  
except: 
  sys.exit(64)

command= os.environ['ATLAS_LOCAL_ROOT_BASE'] + '/utilities/getCricData.sh -q ddmendpointstatus ' + jsonfileDir  
try:
  return_value = os.system(command)  
  if return_value != 0:
      sys.exit(64)
except: 
  sys.exit(64)

jsonfileLocation = jsonfileDir + 'ddmendpointstatus.json'

with open(jsonfileLocation,'r') as jsonfile:
  jsondata = json.load(jsonfile)
jsonfile.close()

ddmInfo=''
lastDdmendpoint=''
for key in jsondata.keys():
  for key1 in jsondata[key].keys():
    if (jsondata[key][key1]['status']['value'] != "ON" ):
      if lastDdmendpoint != jsondata[key][key1]['status']['ddmendpoint']:
        if ddmInfo != '' :
          print(ddmInfo)          
        ddmInfo = str(jsondata[key][key1]['status']['ddmendpoint'])
        lastDdmendpoint = jsondata[key][key1]['status']['ddmendpoint']
      ddmInfo = ddmInfo + '|  activity: ' + str(jsondata[key][key1]['status']['activity']) + '|  expiration: ' + str(jsondata[key][key1]['status']['expiration']) + '|  reason: ' + str(jsondata[key][key1]['status']['reason']) 

if ddmInfo != '' :
  print(ddmInfo)          

sys.exit(0)
