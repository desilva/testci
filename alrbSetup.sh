cd ATLASLocalRootBase
find /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase -maxdepth 1  -type d | awk '{print "[ ! -d `basename "$1"` ] &&  ln -s "$1""}' | sh

export ATLAS_LOCAL_ROOT_BASE=`pwd`
echo $ATLAS_LOCAL_ROOT_BASE
ls -l $ATLAS_LOCAL_ROOT_BASE

return $?
